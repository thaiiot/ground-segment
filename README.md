# 1. Ground Segment Introduction

This document describes the THAIIOT ground station. It is located on the roof of the Electrical and Civil Engineering Department building at NKRAFA.

This ground station will be automated, and will be able to be controlled by manual.

National and international frequency coordination has been achieved through the NBTC (National Broadcasting and Telecommunication Commission) for national frequency coordination and through the ITU (International Telecommunications Union) and the IARU (International Amateur Radio Union) for international coordination.

The assigned frequencies are 145.888 MHz for the uplink and 435.888 MHz and 2250 MHz for the downlink.

# 2 Ground Segment Requirement

This section describes the requirement of THAIIOT ground station with the following

1. This ground station can receive beacon signal from the THAIIOT satellite and other satellites via the UHF antenna.

2. This ground station can transmit command signal to the THAIIOT satellite via the VHF antenna.

3. This ground station can receive beacon signal from the THAIIOT satellite via the UHF antenna.

4. This ground station can receive payload data from S-band signal from the THAIIOT satellite via the S-band antenna.

5. This ground station can track the THAIIOT satellite and other satellites.

# 3 Ground Segment Overview and Specification

This section describes the overview and specification of hardware in THAIIOT ground station. Figure 1 shows the THAIIOT ground station hardware block diagram with the direction of uplink, downlink and control.

![](THAIIOT_Ground_Station_Hardware_Block_Diagram.jpg)

**Figure 1: THAIIOT Ground Station Hardware Block Diagram**

The hardware in THAIIOT ground station separates in 3 categories as follows;

## 3.1 Controller Hardware

The controller hardware consists of 2 sections as follows;

### 3.1.1 VHF and UHF Band

This ground station uses Rot2Prog rotator from SPID Elecktronik which have the specification in table 1.

**Table 1: Specification of Rot2Prog rotator from SPID Elecktronik**

| Features | Value | Unit |
| --- | --- | --- |
| Input Voltage (Typical) | 12 - 24 | V |
| Input Current (Nominal Draw) | 2 - 3 | A |
| Motor | 12 - 24 | V |
| Fuse | 8.0 | AMP GMA |
| Rotation Speed (azimuth) | 120 | Sec (12V) |
| Rotation Speed (elevation) | 80 | Sec (12V) |
| Turning Torque (in-lbs) | 1400 |
 |
| Braking Torque (in-lbs) | \&gt; 14,000 |
 |

### 3.1.2 S-Band

This ground station uses MD-02 rotator from SPID Elecktronik which have the specification in table 2.

**Table 2: Specification of MD-02 rotator from SPID Elecktronik**

| Features | Value | Unit |
| --- | --- | --- |
| Supply voltage of driver MD-02 | 15 | VDC (Imax – 2A) |
| Supply voltage of rotors | 12 - 14 | VDC (Imax – 40 A) |
| The maximum current of a single motor | up to 20 | A |
| Enclosure | Desktop model |
 |
| Controller | Azimuth &amp; Elevation |
 |
| Rotator | All SPID and SPX rotators |
 |
| MTBF | 15000 | Hours @ -5 to +40°C |
| Display | LCD 2\*20 digit (green) |
 |

## 3.2 RF Hardware

The RF hardware consists of 3 sections as follows;

### 3.2.1 VHF Band

This ground station uses SDR (USRP B200) and RF power amplifier for transmitting command which have the specification in table 3 and 4 respectively.

**Table 3: Specification of Transmitter SDR (USRP B200)**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 144 – 150, 400-440 | MHz |
| Sensitivity | -90 | dBm |
| Data rate | 1200, 2400, 4800, 9600 | bps |
| Maximum input signal | 0 | dBm |
| Noise Figure | 15 | dB |

**Table 4: Specification of RF Power Amplifier**

| Features | Value | Unit |
| --- | --- | --- |
| Center Frequency | 145.888 | MHz |
| Minimum Input Power | 10 | dBm |
| Maximum Output Power | 60 | W |
| Power Supply | 220 | V |

### 3.2.2 UHF Band

This ground station uses SDR (AFE822x SDR-Net) for receiving beacon and telemetry which has the specification in table 5.

**Table 5: Specification of Receiver SDR (AFE822x SDR-Net)**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 35 - 1700 | MHz |
| Sensitivity | -143 | dBm |
| RF ADC Sampling Rate | 70.656 | MHz |
| Maximum Receiving Bandwidth | 920 | kHz |
| Noise Figure | 4 - 10 | dB |

### 3.2.3 S-Band

This ground station uses small satellite modem (Teledyne QubeFlex) for receiving payload data which has the specification in table 6.

**Table 6: Specification of Small Satellite Modem (Teledyne QubeFlex)**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 950 - 2450 | MHz |
| Sensitivity | -140 | dBm |
| Data rate | 2.4 - 2048 | kbps |
| Symbol Rate Limit | 2.4 - 40000 | ksps |
| Return Loss | \&gt;12 | dB |

## 3.3 Antenna

The antenna consists of 3 sections as follows;

### 3.3.1 VHF Band

This ground station uses double-crossed Yagi antenna for VHF band from M2 which has the specification in table 7.

**Table 7: Specification of Double-Crossed Yagi Antenna for VHF**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 144 - 148 | MHz |
| Gain | 17.39 | dBi |
| Beamwidth | 38 | Degree |
| Maximum VSWR | 1.4:1 |
 |
| Feed Impedance | 50 | Ohm |
| Feed Type | Folded Dipole |
 |

### 3.3.2 UHF Band

This ground station uses double crossed Yagi antenna for UHF band from M2 which has the specification in table 8.

**Table 8: Specification of Double-Crossed Yagi Antenna for UHF**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 430 - 438 | MHz |
| Gain | 21.9 | dBi |
| Beamwidth | 21 | Degree |
| Maximum VSWR | 1.5:1 |
 |
| Feed Impedance | 50 | Ohm |
| Feed Type | Folded Dipole |
 |

### 3.3.3 S-Band

This ground station uses Dish antenna for S-band from RF HAMDESIGN which has the specification in table 9.

**Table 9: Specification of Dish Antenna for S-band**

| Features | Value | Unit |
| --- | --- | --- |
| Frequency Range | 2400 - 2450 | MHz |
| Gain | 29.2 | dBi |
| Beamwidth | 5.1 | Degree |
| Maximum VSWR | 1.05:1 |
 |
| Feed Impedance | 50 | Ohm |
| Feed Type | LHCP Helix |
 |
